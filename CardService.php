<?php

namespace App\Services\Cards;

use App\Helpers\StringConverter;
use App\Storage\Interfaces\CardRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class CardService
{
    /**
     * Return records data.
     *
     * @param string $cardNumber
     * @param string $bank
     * @param string|null $comment
     * @return void
     */
    public function store(string $cardNumber, string $bank, ?string $comment): void
    {
        app(CardRepositoryInterface::class)->create([
            'pay_in_operator_id' => Auth::user()->payInOperator->id,
            'date' => now(),
            'card_number' => StringConverter::removeSpacesFromString($cardNumber),
            'bank' => $bank,
            'comment' => $comment,
        ]);
    }

    /**
     * Activate a selected bank card.
     *
     * @param int $cardId
     * @return void
     */
    public function activateCard(int $cardId): void
    {
        $cardRepository = app(CardRepositoryInterface::class);

        $this->deactivateAllCards();
        $card = $cardRepository->find($cardId);
        $card->is_active = true;
        $cardRepository->save($card);
    }

    /**
     * Deactivate all bank cards.
     *
     * @return void
     */
    protected function deactivateAllCards(): void
    {
        $cardRepository = app(CardRepositoryInterface::class);

        $cardRepository->updateRelation(auth()->user()->payInOperator->cards(), [
            'is_active' => false,
        ]);
    }
}
