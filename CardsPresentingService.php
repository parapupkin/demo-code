<?php

namespace App\Services\Cards;

use App\Models\Card;
use App\Scopes\CardRegistryScope;
use App\Services\Presenters\Cards\InternalExecutorPresenter;
use App\Storage\Interfaces\CardRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CardsPresentingService
{
    /**
     * Amount of items on a page.
     */
    protected const ITEMS_PER_PAGE = 10;

    /**
     * Records for a certain page.
     *
     * @var LengthAwarePaginator
     */
    protected LengthAwarePaginator $paginator;

    /**
     * PayIn operator id.
     *
     * @var int|null
     */
    protected ?int $payInOperatorId;

    /**
     * Presenter.
     *
     * @var InternalExecutorPresenter
     */
    private InternalExecutorPresenter $presenter;

    /**
     * Scope.
     *
     * @var CardRegistryScope
     */
    private CardRegistryScope $scope;

    /**
     * Constructor.
     */
    public function __construct(
        CardRegistryScope $scope,
        InternalExecutorPresenter $presenter
    ) {
        $this->scope = $scope;
        $this->presenter = $presenter;
    }

    /**
     * Return records data.
     *
     * @param array $orderConfigArray
     * @param string|null $searchConfig
     * @return Collection
     */
    public function getRegistryCards(array $orderConfigArray, ?string $searchConfig): Collection
    {
        $cardRepository = app(CardRepositoryInterface::class);
        $this->scope->setConfigForApply($this->payInOperatorId, $orderConfigArray, $searchConfig, $this->presenter);
        $request = request();
        $cardsQuery = $cardRepository->getScopedQuery($this->scope);
        $this->paginator = $cardsQuery->paginate($request->length ?? self::ITEMS_PER_PAGE);

        return $this->paginator->getCollection()->map(function (Card $item) {
            $this->presenter->setModel($item);
            return $this->presenter->getValues();
        });
    }

    public function getTotalFromPaginator(): int
    {
        return $this->paginator->total();
    }

    /**
     * Set operator id.
     *
     * @param int $payInOperatorId
     */
    public function setPayInOperator(int $payInOperatorId): void
    {
        $this->payInOperatorId = $payInOperatorId;
    }

    /**
     * Get presenter.
     *
     * @return InternalExecutorPresenter
     */
    public function getPresenter(): InternalExecutorPresenter
    {
        return $this->presenter;
    }
}
